# Get Video Bot

Telegram bot written in Rust to get a video download of a YouTube video.

## Building

1. Built using Rust 1.65.0
2. Clone repository
3. Run `cargo run` inside of the local repository
4. Enjoy!

## Known Issues

Uploading a file bigger than ~100MiB

## Contributing

MRs are currently accepted, for right now just follow the format of `[Feature]: Fix message` 
ex. `[YouTube]: Fix upload error`.

## License 

Licensed under GPLv3