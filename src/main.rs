use std::path::Path;
use teloxide::{prelude::*, utils::command::BotCommands};
use std::process::Command as Process;
use teloxide::types::InputFile;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    log::info!("Starting get_vid bot...");

    let bot = Bot::from_env();

    Command::repl(bot, answer).await;
}

#[derive(BotCommands, Clone, Debug)]
#[command(rename_rule = "lowercase", description = "These commands are supported:")]
enum Command {
    #[command(description = "Displays this menu")]
    Help,
    #[command(description = "Get a YouTube video (Uploaded as mp4)")]
    YouTube {
        link: String
    }
}

async fn answer(bot: Bot, msg: Message, cmd: Command) -> ResponseResult<()> {
    match cmd {
        Command::Help => bot.send_message(msg.chat.id, Command::descriptions().to_string()).await?,
        Command::YouTube {link} => {
            log::info!("Sending video...");
            let video = get_video(&link).await.expect("TODO: panic message");
            let id = video.id();
            let path = format!("./cache/{id}");
            if !Path::new(format!("./cache/{id}.mp4").as_str()).exists() {
                log::info!("File does not exist, creating...");
                // Using yt-dlp to download the video for now
                Process::new("yt-dlp")
                    // .args(["-o", "./cache/%(id)s.%(ext)s", link.as_str(), "--recode-video", format.as_str()])
                    .args(["-o", "./cache/%(id)s.%(ext)s", link.as_str(), "--recode-video", "mp4"])
                    .output()
                    .expect("failed to execute process");
            }
            let file_path = InputFile::file(format!("./cache/{id}.mp4"));
            log::info!("Video sent successfully!");
            bot.send_video(msg.chat.id, file_path).await?
        }
    };

    Ok(())
}

async fn get_video(link: &String) -> Result<ytextract::Video, Box<dyn std::error::Error>> {
    let client = ytextract::Client::new();
    let video = client.video(link.parse()?).await?;
    Ok(video)
}